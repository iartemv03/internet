#pragma once
#include <string>
#define MAX_STRING_SIZE 200
using namespace std;
struct date
{
    int hour;
    int min;
    int sec;
};

struct byte
{
    int bytein;
    int byteout;
};

struct path
{
    string path;
};
struct protocol
{
    byte Byte;
    date start;
    date finish;
    path Path;
    char title[MAX_STRING_SIZE];
};