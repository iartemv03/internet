#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "protocol1.h"

void read(const char* file_name, protocol* array[], int& size);

#endif