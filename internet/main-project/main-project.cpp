#include <iostream>

using namespace std;

#include "protocol1.h"
#include "file_reader.h"
#define MAX_FILE_ROWS_COUNT 100
int main()
{
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #5. Internet protocol\n";
    cout << "Author: Artem Imennoi\n";
    cout << "Group: 11\n";
    protocol* elem[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", elem, size);
        for (int i = 0; i < size; i++)
        {
            cout << "����� ������ ������........: ";
            cout << elem[i]->start.hour << '\n';
            cout << elem[i]->start.min << '\n';
            cout << elem[i]->start.sec << '\n';
            cout << "����� ����� ������........: ";
            cout << elem[i]->finish.hour << ' ';
            cout << elem[i]->finish.min << ' ';
            cout << elem[i]->finish.sec << '\n';
            cout << "���������� ���������� ������........: ";
            cout << elem[i]->Byte.bytein << ' ';
            cout << "���������� �������� ������........: ";
            cout << elem[i]->Byte.byteout << ' ';
            cout << "����........: ";
            cout << elem[i]->Path.path << '\n';
            cout << elem[i]->title << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete elem[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }

    cout << "Group: 11\n";

    return 0;
}

